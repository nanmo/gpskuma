GPS瞬間移動クマ　リポジトリ簡易説明

■ライセンス
MITライセンス

■ファイル/フォルダ構成

/bear_transport_se.flp
効果音作成時に使用したFL Studioのプロジェクトファイルです。
Flashのプロジェクトファイルではありません。

/bear_wave_trans_anim
画像素材作成用のフォルダです。
各素材や、アニメーション作成時のAfterEffectsプロジェクトファイル、
シェルスクリプトを含みます。

/dev
プログラム用フォルダです。
index.htmlやgame.js,ゲームに実際に使用する画像/音声素材などが含まれます。