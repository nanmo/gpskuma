enchant();

var bear_transport_se = new Audio("bear_transport_se.mp3");
bear_transport_se.load();

//watchPosition用
var watch_tracker_id
var watch_latitude = 0;
var watch_longitude = 0;

//debug loop
var debug_loop = false;

window.onload = function() {
    var Rectangle = enchant.Class.create({
        initialize: function(x, y, width, height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        },
        right: {
            get: function() {
                return this.x + this.width;
            }
        },
        bottom: {
            get: function() {
                return this.y + this.height;
            }
        }
    });
    var game = new Game(320, 320);
    game.fps = 15;
    game.preload('bear_wave.png');
    game.onload = function() {
		//タイトルというか説明
		var title = new Label('');
		title.color="#FFF";
		title.y = 80;
		title.prefix = '<div style="text-align:center;font-size:29px;width:320px;">';
		title.suffix =  '</div>';
		title.text = title.prefix + "クマをタッチすると瞑想します、もう一度タッチすると瞑想を解いて瞬間移動します" + title.suffix;
		
		var bear = new Sprite(20, 30);
		bear.x = 150;
		bear.y = 145;
		bear.zen = false;
		bear.latitude = 0;
		bear.longitude = 0;
		bear.counter = 0;
		bear.distance = 0;
		bear.image = game.assets['bear_wave.png'];
		bear.touchendHandler = function() {
			if ( bear.zen )
			{
				bear_transport_se.volume = 1;
				bear_transport_se.play();
				if ( !debug_loop )
				{
					bear.removeEventListener('touchend', bear.touchendHandler );
				}
				navigator.geolocation.clearWatch(watch_tracker_id);
				//http://keisan.casio.jp/has10/SpecExec.cgi
				bear.distance =  parseInt(1000 * 6400 * Math.acos( Math.sin( degreeToRadian( bear.longitude ) ) * Math.sin( degreeToRadian( watch_longitude ) )  + Math.cos( degreeToRadian( bear.longitude ) ) * Math.cos( degreeToRadian( watch_longitude ) ) * Math.cos( degreeToRadian( bear.latitude - watch_latitude ) ) ) );
				//title.text = bear.distance;
				bear.addEventListener('enterframe', function() {
					if ( bear.counter > 4  )
					{
						title.text = title.prefix + parseInt( bear.distance * bear.counter / 24 ) + "m" + title.suffix;
					}
					else
					{
						title.text = "";
					}

					if ( bear.counter < 2 )
					{
						bear.frame++;
					}
					else if ( bear.counter == 20 )
					{
					bear.visible = true;
					}
					else if ( bear.counter > 20 && bear.counter < 24)
					{
						bear.frame++;
					}
					else if ( bear.counter == 24 )
					{
						//title.text = title.prefix + bear.distance + "m" + title.suffix;
						//bear.visible = true;
						bear.counter = 0;
						//bear.frame = 0;
						//bear.zen = false;
						bear.removeEventListener('enterframe', arguments.callee, false );
						bear.generateScore();
						return;
					}
					else
					{
						bear.visible = false;
					}
					bear.counter++;
				});
			}
			else
			{
				//bear_transport_se.currentTime = 2000;
				bear_transport_se.volume = 0;
				//bear_transport_se.pause();
				//bear_transport_se.load();
				bear_transport_se.play();
				bear_transport_se.pause();
				bear.frame = 3;
				title.text = "";
				navigator.geolocation.getCurrentPosition(function (position) {
					bear.zen = true;
					bear.latitude = position.coords.latitude;
					bear.longitude = position.coords.longitude;
					watch_latitude = bear.latitude;
					watch_longitude = bear.longitude;
					//title.text = bear.latitude + ":" + bear.longitude;
					title.text = title.prefix + "瞬間移動前の瞑想中……もう一度クマをタッチすると瞑想を解いて瞬間移動します" + title.suffix;
					watch_tracker_id = navigator.geolocation.watchPosition( function (position) {
						watch_latitude = position.coords.latitude;
						watch_longitude = position.coords.longitude;
						pos_debug.text = '<div style="text-align:center;">' + watch_latitude + "." + watch_longitude + '</div>';
					});
				}, function(){alert("GPSが使えません")});
			}
		};
		bear.generateScore = function () {
				var random = Math.random();
				var score_text = "クマが瞬間移動で " + this.distance + " メートル移動し";
				bear.zen = false;
				if ( random < 0.05 )
				{
					this.frame = 0;
					score_text += "、その後爆発して消え去りました。";
					setTimeout( function() {
						bear.frame = 15;
						bear.addEventListener('enterframe', function() {
							if ( this.frame == 19 )
							{
								bear.removeEventListener('enterframe', arguments.callee, false );
								bear.visible = false;
								//game.end(this.distance, score_text)
							}
							else
							{
								this.frame++
							}
						});
					}, 250);
					//return;
				}
				else if ( random < 0.3 )
				{
					this.frame = 3;
					score_text += "、目が細くなりました。"
				}
				else if ( random < 0.4 )
				{
					this.frame = 10;
					score_text += "、白クマに変化しました。";
				}
				else if ( random < 0.5 )
				{
					this.frame = 11;
					score_text += "、雌クマに変化しました。";
				}
				else if ( random < 0.6 )
				{
					this.frame = 12;
					score_text += "、スケボーに乗りました。";
				}
				else if ( random < 0.65 )
				{
					this.frame = 13;
					score_text += "、人間の男に進化しました。";
				}
				else if ( random < 0.7 )
				{
					this.frame = 14;
					score_text += "、人間の女に進化しました。";
				}
				else
				{
					this.frame = 0;
					score_text += "ました。";
				}
				if ( !debug_loop )
				{
					//bear.removeEventListener('touchend', bear.touchendHandler );
					setTimeout( function() { game.end(this.distance, score_text) }, 1000 );
				}
				else
				{
					pos_debug.text = score_text;
				}
		};
		bear.addEventListener('touchend',  bear.touchendHandler);
		var pos_debug = new Label('');
		pos_debug.color = "#888";
		pos_debug.y = 8;

		game.rootScene.addChild(pos_debug);
		game.rootScene.addChild(title);
		game.rootScene.addChild(bear);
        game.rootScene.backgroundColor = 'rgb(0, 0, 0)';
    }    
    game.start();
};

function degreeToRadian ( degree )
{
	return degree * Math.PI / 180;
}